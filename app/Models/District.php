<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use HasFactory;
    /**
     * Get the division that owns the District
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function division(): BelongsTo
    {
        return $this->belongsTo(Division::class);
    }

    /**
     * Get all of the thanas for the District
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function thanas(): HasMany
    {
        return $this->hasMany(Thana::class);
    }
}
