<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Thana extends Model
{
    use HasFactory;
    

    /**
     * Get the districts that owns the Thana
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function districts(): BelongsTo
    {
        return $this->belongsTo(District::class);
    }
}
