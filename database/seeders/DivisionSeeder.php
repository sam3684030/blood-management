<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DivisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('divisions')->insert([
            'div_name' => 'Barisal',
        ]);
        DB::table('divisions')->insert([
            'div_name' => 'Chittagong',
        ]);
        DB::table('divisions')->insert([
            'div_name' => 'Dhaka',
        ]);
        DB::table('divisions')->insert([
            'div_name' => 'Khulna',
        ]);
        DB::table('divisions')->insert([
            'div_name' => 'Mymensingh',
        ]);
        DB::table('divisions')->insert([
            'div_name' => 'Rajshahi',
        ]);
        DB::table('divisions')->insert([
            'div_name' => 'Rangpur',
        ]);
        DB::table('divisions')->insert([
            'div_name' => 'Sylhet',
        ]);
    }
}
