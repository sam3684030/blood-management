<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('districts')->insert([
            'dis_name' => 'Bhola',
            'div_id' => 1,
        ]);
        DB::table('districts')->insert([
            'dis_name' => 'Jhalokati',
            'div_id' => 1,
        ]);

        DB::table('districts')->insert([
            'dis_name' => 'Cox\'s Bazar',
            'div_id' => 2,
        ]);
        DB::table('districts')->insert([
            'dis_name' => 'Rangamati',
            'div_id' => 2,
        ]);

        DB::table('districts')->insert([
            'dis_name' => 'Dhaka',
            'div_id' => 3,
        ]);
        DB::table('districts')->insert([
            'dis_name' => 'Tangail',
            'div_id' => 3,
        ]);

        DB::table('districts')->insert([
            'dis_name' => 'Chuadanga',
            'div_id' => 4,
        ]);
        DB::table('districts')->insert([
            'dis_name' => 'Jessore',
            'div_id' => 4,
        ]);
        
        DB::table('districts')->insert([
            'dis_name' => 'Mymensingh',
            'div_id' => 5,
        ]);
        DB::table('districts')->insert([
            'dis_name' => 'Netrokona',
            'div_id' => 5,
        ]);

        DB::table('districts')->insert([
            'dis_name' => 'Joypurhat',
            'div_id' => 6,
        ]);
        DB::table('districts')->insert([
            'dis_name' => 'Naogaon',
            'div_id' => 6,
        ]);

        DB::table('districts')->insert([
            'dis_name' => 'Gaibandha',
            'div_id' => 7,
        ]);
        DB::table('districts')->insert([
            'dis_name' => 'Kurigram',
            'div_id' => 7,
        ]);

        DB::table('districts')->insert([
            'dis_name' => 'Moulvibazar',
            'div_id' => 8,
        ]);
        DB::table('districts')->insert([
            'dis_name' => 'Sunamganj',
            'div_id' => 8,
        ]);
        
    }
}
